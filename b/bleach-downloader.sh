#!/bin/sh

TARGET_DIR=${TARGET_DIR:-$HOME/Downloads/bleach}
EXTRACT_DIR=$TARGET_DIR/extracted
COMMON_URL="http://www.bleachportal.it/dl/bleach_episodi"
EXTRACTOR=${EXTRACTOR:-~/Downloads/7zz}

if command -v axel >/dev/null 2>&1; then
	downloader="axel -o $TARGET_DIR"
else
	curl() {
		url=$1
		filename=$(getfilename_from_url "$url")
		#url=${url// /%20} # In POSIX str replacement is undefined
		url=$(echo "$url" | sed 's/ /%20/g')
		echo "curl -L \"$url\" -o \"$filename\""
	}

	downloader=curl
fi


SD_SEQ="$(seq 54 67) 68-69 $(seq 70 72) 73-74 75-76 $(seq 77 83) 84-85 $(seq 86 167)"
AVI_HQ_SEQ="$(seq 168 175) 176-177 $(seq 178 198) 200"
MP4_HD_SEQ="199 $(seq 201 366)"

getfilename() {
	episode=$1
	quality=$2
	filename="Bleach - $episode"
	[ "$quality" != 'SD' ] && filename="$filename $quality"
	echo "$filename"
}

getfilename_from_url() {
	url=$1
	echo "$TARGET_DIR/$(echo "$url" | grep -Eo 'Bleach - \S+.zip')"
}

# If already exists, do not download
check_existence() {
	ep=$1
	quality=$2
	filename="$EXTRACT_DIR/Bleach - $ep"
	[ "$quality" = 'HD' ] && filename="$filename $quality"
	[ -f "$filename.mp4" ] || [ -f "$filename.avi" ] && return 0
	return 1
}


main() {
	sequence=$1
	quality=$2

	for ep in $sequence; do
		check_existence "$ep" "$quality" && continue
		filename=$(getfilename "$ep" "$quality")
		url="$COMMON_URL/$quality/$filename.zip"
		$downloader "$url" && \
			# $EXTRACTOR -p"bleach$ep" -o"$EXTRACT_DIR" x "$(getfilename_from_url "$url")" && \ #
			$EXTRACTOR -p"bleach$ep" -o"$EXTRACT_DIR" x "${filename##*/}.zip" && \
			printf "\n\nEpisode %s finished.\n" "$ep"
		sleep 3
	done
}

main "$SD_SEQ" 'SD'
main "${AVI_HQ_SEQ}" 'HQ'
main "${MP4_HD_SEQ}" 'HD'
