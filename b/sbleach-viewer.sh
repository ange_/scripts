#!/bin/bash -e

if [[ $(id -u) -eq 0 ]]; then
	echo "This script cannot be run as root" >&2
	exit 1
fi

## Useful functions ##

usage(){
	echo "Usage: ${0##*/} [NR° EPISODE] "
	echo -e "\n\t--help\tShows this Usage message"
	echo -e "\t--last-episode\tShows the Last seen Episode"
	echo -e "\t--next-episode\tOpen the Next Episode in the browser"
	echo -e "\t--browser [BROWSER]\tSets a different browser"
	echo -e "\n\tAny other input will be considered as an error"
}

isDebug(){
	for i in $(seq 1 $#); do
		if [[ $i == "--debug" ]]; then
			return true;
		fi
	done
	return false;
}

if [ "$(uname)" == "Darwin" ]; then
	#config_path="$HOME/Library/bleach-viewer"
	if [[ isDebug ]]; then
		config_path="$HOME/bleach-viewer"
	else
		config_path="$HOME/Desktop/Support/bleach-viewer"
	fi
else
	config_path="$HOME/.config/bleach-viewer/"
fi

echo $config_path

seen_episodes="$config_path/seen_episodes.txt"
browser_path="$config_path/browser.cfg"


[[ ! -e $config_path ]] && mkdir -p $config_path

[[ ! -e $seen_episodes ]] && touch $seen_episodes

if [[ ! -e $browser_path ]]; then
	touch $browser_path

	if [ "$(uname)" == Darwin ]; then
		echo "open -a safari" > $browser_path
	else
		if [[ -e /etc/debian_version ]]; then
			echo "x-www-browser" > $browser_path
		elif [[ -e /usr/bin/chromium ]]; then
			echo "/usr/bin/chromium --incognito" > $browser_path
		else
			echo "xdg-open" > $browser_path
		fi
	fi
fi

browser="$(cat $browser_path)"

## Colors ##
export Z="\e[0m"
export GREEN="\e[0;32m"
export LRED="\e[1;31m"

hollow_test(){
	if [[ $1 -eq 19 || $1 -eq 59 || $1 -eq 82 || $1 -eq 112 || $1 -eq 114 || $1 -eq 121 || $1 -eq 122 || $1 -eq 123 || $1 -eq 271 ]]; then
		echo -e "\n${LRED}Hollow episode$Z"
	fi
}

saga(){
	echo -ne "$GREEN"
	if [[ $1 -ge 1 && $1 -le 19 ]]; then
		echo "SAGA INTRODUTTIVA"
	elif [[ $1 -ge 20 && $1 -le 41 ]]; then
		echo "SAGA DELLA SOUL SOCIETY - INFILTRAZIONE"
	elif [[ $1 -ge 42 && $1 -le 63 ]]; then
		echo "SAGA DELLA SOUL SOCIETY - IL SALVATAGGIO DI RUKIA"
	elif [[ $1 -ge 64 && $1 -le 109 ]]; then
		echo "FILLER - SAGA DEI BOUNTOU"
	elif [[ $1 -ge 110 && $1 -le 131 ]]; then
		echo "SAGA DEGLI ARRANCAR - APPARIZIONE"
	elif [[ $1 -ge 132 && $1 -le 151 ]]; then
		echo "SAGA DEGLI ARRANCAR - INFILTRAZIONE NELL'HUECO MUNDO"
	elif [[ $1 -ge 152 && $1 -le 167 ]]; then
		echo "SAGA DEGLI ARRANCAR - LA BATTAGLIA"
	elif [[ $1 -ge 168 && $1 -le 189 ]]; then
		echo "FILLER - SAGA DEL CAPITANO AMAGAI"
	elif [[ $1 -ge 190 && $1 -le 205 ]]; then
		echo "HUECO MUNDO - ATTO SECONDO"
	elif [[ $1 -ge 206 && $1 -le 229 ]]; then
		echo "SAGA DEL PENDOLO"
	elif [[ $1 -ge 230 && $1 -le 265 ]]; then
		echo "FILLER - SAGA DELLE ZANPAKUTO"
	elif [[ $1 -ge 266 && $1 -le 310 ]]; then
		echo "SAGA DELL'INVASIONE DI KARAKURA"
	elif [[ $1 -ge 311 && $1 -le 316 ]]; then
		echo "FILLER"
	elif [[ $1 -ge 317 && $1 -le 341 ]]; then
		echo "SAGA DEI REIGAI"
	elif [[ $1 -ge 342 && $1 -le 366 ]]; then
		echo "SAGA DEI FULLBRING"
	fi
	echo -ne "$Z"
}

already_seen(){
	if [[ $(grep $1 $seen_episodes) != "" ]]; then
		read -p "Episodio già visto, vuoi rivederlo? [S/n] " -n 1 ep_seen
		if [[ $ep_seen == n || $ep_seen == N ]]; then
			echo -e "\nEpisode already seen, it will be considered as an Input Error." >&2
			exit 2
		fi

		unset ep_seen
	fi
}

ep_test(){
	saga $1
	hollow_test $1
}

see_episode(){
	if [[ $1 -gt 265 ]]; then
		$browser http://www.bleachportal.it/streaming/bleach2/stream_bleach_ep$1.htm 2>&1 &
	elif [[ $1 -le 265 ]]; then
		$browser http://www.bleachportal.it/streaming/bleach/stream_bleach_ep$1.htm 2>&1 &
	else
		echo "Episode doesn't exists."
		exit 5
	fi

	echo -n "Episode nr° $1 - "
	ep_test $1
}


last_episode(){
	echo "The last seen episode you've seen is nr° $(tail -1 $seen_episodes)"
	ep_test $(tail -1 $seen_episodes)
}


next_episode(){
	let K=$(tail -1 $seen_episodes)+1
	echo $K >> $seen_episodes
	see_episode $K
	
	unset K
}

index(){
	$browser http://bam.forumcommunity.net/?t=54397599 2>&1 &
}

browser(){
	if [[ -z $1 ]]; then
		echo "You must give an argument to \"--browser\""
	else
		if [ "`zsh -c \"where $1\"`" != "$1 not found" ]; then
			echo "$@" > $browser_path
		else
			echo "\"$1\" does not exits" >&2
		fi
	fi
}

menu(){
	while true; do
		echo -e "\033[1mBleach Viewer\033[0m\n"
		echo -e "[1] Next Episode\t[2] Last Episode\t[3] Review Last Episode\n[4] Custom Episode\t[5] Index\t\t[6] Change Browser"
		echo -e "\n[0] Exit\n"
		read -p "Insert your option: " -n 1 answer
		echo
		case $answer in
			1)
				next_episode
				;;
			2)
				last_episode
				;;
			3)
				episode=$(tail -1 $seen_episodes)
				ep_test $episode
				$browser $episode
				
				unset episode
				;;
			4)
				read -p "Episode nr°: " eps
				#do something with eps
				if [[ $eps -gt 366 || $eps -lt 1 ]]; then
					##do smtg
					echo "Incorrect Input."
				else
					already_seen $eps
					ep_test $eps
					see_episode $eps
				fi
				unset eps
				;;
			5)
				index
				;;
			6)
				read -p "New Browser: " nbrowser
				browser $nbrowser
				echo -e "\nRestart the script now."
				exit 0
				;;
			0)
				exit 0
				;;
			"")
				;;
			*)
				echo -e "[ ${LRED}ERROR$Z ] Incorrect Input" >&2
				;;
		esac
		unset answer
	done
}

## Input test ##

case $1 in
	"--help")
		usage
		exit 0
		;;
	"--last-episode")
		last_episode
		;;
	"--next-episode")
		next_episode
		;;
	"--index")
		index
		;;
	"--browser")
		browser ${@%$1}
		;;
	"")
		menu
		;;
	*)
		if [[ $1 -gt 366 || $1 -lt 1 ]]; then
			echo "Incorrect Input." >&2
			exit 2
		else
			already_seen $1
			ep_test $1
			see_episode $1
		fi
		;;
esac

exit 0
