#!/bin/sh
#License: BSD-3

OLE_LINK="$1"
cookie="$2"
msess="MoodleSession=$cookie"

links="$(curl -kb "$msess" "$OLE_LINK" | xmllint --html --xpath "//li[starts-with(@id, 'section-')]/div[2]/ul/li/div/div[1]/div/div[1]/div/div[2]/div[2]/a/@href" 2>/dev/null -)"

for n in $(seq "$(($(echo "$links" | wc -w) + 1))"); do
	link="$(echo "$links" | cut -d' ' -f "$n")"
	if ! echo "$link" | grep -q "forum"; then
		link="$(echo "$link" | sed -E 's/href=|["]//g')"
		if echo "$link" | grep -q 'folder/'; then
			echo "[ FOLDER ] $link"
			dirlink=$(echo "$link" | sed 's/view.php/download_folder.php/')
			dirname="$(curl -ksI -b "$msess" "$dirlink" | grep -Eo "Content-Disposition: [^\r]+" | cut -d"'" -f3 | tr -d '\r')"

			[ -e "$dirname" ] && dirname="${dirname%.zip}-$n.zip"
			curl -ksLJ -b "MoodleSession=$cookie" "$dirlink" -o "$dirname"
		elif echo "$link" | grep -q 'resource/'; then
			echo "[ FILE ] $link"
			curl -ksLJO -b "$msess" "$link"
		elif echo "$link" | grep -q 'mod/url/'; then
			echo "[ LINK ] $link"
			page="$(curl -ksLJ -b "$msess" "$link")"
			phead="$(echo "$page" | xmllint --html --xpath "//h1/text()" - 2>/dev/null)"
			pdesc="$(echo "$page" | xmllint --html --xpath "//div[@class='no-overflow']/p/text()" - 2>/dev/null)"
			plink="$(echo "$page" | xmllint --html --xpath "//div[@class='urlworkaround']/a/text()" - 2>/dev/null)"
			printf "%s\t%s\t%s\n" "$phead" "$pdesc" "$plink"
			cat <<- EOF > "$phead.txt"
				$pdesc
				$plink
			EOF
		elif echo "$link" | grep -q 'mod/book/'; then
			echo "[ BOOK ] $link"
			echo "$link" >> books.txt
		else
			echo "[ UNKNOWN ] $link"
		fi
	fi
done
