#!/bin/sh

active_connections=$(dbus-send --system --print-reply --dest=org.freedesktop.NetworkManager /org/freedesktop/NetworkManager org.freedesktop.DBus.Properties.Get string:'org.freedesktop.NetworkManager' string:'ActiveConnections' | awk '/ActiveConnection/ { print $3 }' | tr -d '"')

devices=$(dbus-send --system --print-reply --dest=org.freedesktop.NetworkManager /org/freedesktop/NetworkManager org.freedesktop.DBus.Properties.Get string:'org.freedesktop.NetworkManager' string:'AllDevices' | awk '/object path/ { print $3 }' | tr -d '"')

settings=$(dbus-send --system --print-reply --dest=org.freedesktop.NetworkManager /org/freedesktop/NetworkManager/Settings org.freedesktop.DBus.Properties.Get string:'org.freedesktop.NetworkManager.Settings' string:'Connections' | awk '/object path/ { print $3 }' | tr -d '"')

IP4Config_from_ActiveConnection() {
	ac="$1"
	dbus-send --system --print-reply --dest=org.freedesktop.NetworkManager "$ac" org.freedesktop.DBus.Properties.Get string:'org.freedesktop.NetworkManager.Connection.Active' string:'Ip4Config' | awk '/IP4Config/ { print $4 }' | tr -d '"'
}

IP4Config_from_device() {
	dev="$1"
	dbus-send --system --print-reply --type=method_call --dest=org.freedesktop.NetworkManager "$dev" org.freedesktop.DBus.Properties.Get string:'org.freedesktop.NetworkManager.Device' string:'Ip4Config' | awk '/object path/ { print $4 }' | tr -d '"'
}

Device_from_ActiveConnection() {
	ac="$1"
	dbus-send --system --print-reply --type=method_call --dest=org.freedesktop.NetworkManager "$ac" org.freedesktop.DBus.Properties.Get string:'org.freedesktop.NetworkManager.Connection.Active' string:'Devices' | awk '/Devices/ { print $3 }' | tr -d '"'
}

iface_from_Device() {
	dev="$1"
	dbus-send --system --print-reply --type=method_call --dest=org.freedesktop.NetworkManager "$dev" org.freedesktop.DBus.Properties.Get string:'org.freedesktop.NetworkManager.Device' string:'Interface' | awk '/string/ { print $3}' | tr -d '"'
}

get_settings_type() {
	s="$1"
	dbus-send --system --print-reply --dest=org.freedesktop.NetworkManager "$s" org.freedesktop.NetworkManager.Settings.Connection.GetSettings | awk '/string "type"/ { getline; print $3 }'
}

echo '[ ActiveConnections ]'
for ac in $active_connections; do
	device="$(Device_from_ActiveConnection "$ac")"
	cat << EOF
Found $ac
	IP4Config: $(IP4Config_from_ActiveConnection "$ac")
	Device: $device
	Interface: $(iface_from_Device "$device")
EOF
done

echo
echo '[ Devices ]'
for dev in $devices; do
	cat << EOF
Found $dev
	IP4Config: $(IP4Config_from_device "$dev")
	Interface: $(iface_from_Device "$dev")
EOF
done

echo
echo '[ Settings ]'
for setting in $settings; do
	cat << EOF
Found $setting
	Type: $(get_settings_type "$setting")
EOF
done
