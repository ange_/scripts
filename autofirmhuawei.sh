#!/bin/sh

for dependency in curl xmllint cut tr; do
	if ! command -v "$dependency" >/dev/null 2>&1; then
		echo "Missing: $dependency"
		exit 1
	fi
done

curl http://huawei-firmware.com/phone-list/huawei-p8-lite/ale-l21 | xmllint --html --xpath "//tbody/tr/td/font/strong/a/@href" 2>/dev/null - | tr ' ' '\n' | while read -r line; do
	url=$(echo "$line" | cut -d '"' -f2)
	country=$(curl "$url" | xmllint --html --xpath "//div[3]/span/strong/text()" 2>/dev/null -)
	echo "$country"
	[ "$country" = 'Country: Italy' ] && echo "$url" >> ~/valid_urls.txt
done
