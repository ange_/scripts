#!/bin/sh

INSTALL_PATH=${INSTALL_PATH:-$HOME/.local/opt}

get_latest_version() {
	printf '%s\n%s\n' "$1" "$2" | sort -V | head -1
}

get_filename() {
	arch="-$(uname -m)"
	os="$(uname | tr '[:upper:]' '[:lower:]')"
	if [ "$os" = 'darwin' ]; then
		os='macos'
	elif [ "$os" = 'linux' ]; then
		arch='64'
	fi
	echo "nvim-${os}${arch}.tar.gz"
}

update_nvim() {
	last_version_available="$(curl https://github.com/neovim/neovim/releases/tag/stable | xmllint --html --xpath '//div/pre/code' - 2>/dev/null | awk '/NVIM v[^\n]+/ { print $2; }' | tr -d '\r')"
	current_version_installed=$(nvim --version 2>/dev/null | head -1 | cut -f2 -d' ')

	if [ "$current_version_installed" != "$last_version_available" ]; then 	# A dev build would be replaced without an explicit check
		echo "Current version installed: ${current_version_installed:-Not installed}"
		echo "Last version available	: $last_version_available"

		if [ "$current_version_installed" = "$(get_latest_version "$current_version_installed" "$last_version_available")" ]; then
			echo 'Ok, upgrading...'

			work_dir=$(mktemp -d)
			echo "$work_dir"
			cd "$work_dir" || return 12

			# Get download links
			filename="$(get_filename)"
			remote_files="$(curl -s https://api.github.com/repos/neovim/neovim/releases/latest | awk -F': ' "/$filename/ && /browser_download_url/ { gsub(/\"/, \"\", \$2); print \$2 }")"	# I wanted to use ''-quotes and -v, but it didn't work.

			for remote_file in $remote_files; do
				printf "\nDownloading %s...\n" "$(basename "$remote_file")"
				curl -LJO "$remote_file"
			done

			shasum -a 256 -c -- *.sha256sum
			root_dir="$(find . -name "*.tar.gz" -print0 | xargs tar tf | sort | head -1 | xargs dirname)"
			echo "Tar's root directory: $root_dir"

			tar xf ./*.tar.gz
			[ ! -d "$INSTALL_PATH" ] && mkdir -p "$INSTALL_PATH"

			mv "$root_dir" "$INSTALL_PATH/nvim"
			unlink "$HOME/bin/nvim"
			ln -s "$INSTALL_PATH/nvim/bin/nvim" "$HOME/bin/nvim" 
		else
			echo 'Already up-to-dated.' >&2
		fi
	else
		echo 'Nothing to do.'
	fi
}

update_nvim
  
