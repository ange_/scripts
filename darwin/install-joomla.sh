#!/bin/sh

err() {
	ec="$1"
	shift
	echo "$@" >&2
	exit "$ec"
}

[ "$#" -gt 2 ] && err 1 "usage: $(basename "$0") <target> [<tar-path>]"

joomla_target="$1"
joomla_target_basename="$(basename "$joomla_target")"

if [ "$#" -eq 2 ]; then
	joomla_archive="$(realpath "$2")"
else
	AUTODETECT_PATH="$HOME/Downloads"
	joomla_archive="$(find "$AUTODETECT_PATH" -name Joomla\* | sort -rV | head -1)"
fi

mkdir -p "$joomla_target" || err 2 "unable to create \"$joomla_target\"."
cd "$joomla_target" || err 3 "unable to cd into \"$joomla_target\"."
tar xf "$joomla_archive" || err 4 "unable to extract \"$joomla_archive\"."

cd ..
chmod -R +a "_www allow write,execute" "$joomla_target_basename"
printf "This script will change \"%s\"'s group to _www in order to ease up some configuration details.\nPlease, enter your " "$joomla_target_basename"
sudo chgrp -R _www "$joomla_target_basename" || err 5 'Fatal error: a valid password is required to proceed.'
cd "$joomla_target_basename"

chmod -R 777 administrator/cache administrator/logs
chmod -R 777 tmp
chmod -R 777 images media language
chmod -R g+w "$joomla_target"
find installation \( -type f -o -type d \) -exec chmod g+w '{}' \;

echo 'DirectoryIndex index.php index.html' > "${joomla_target}/.htaccess"

cat << EOF > "${joomla_target}/composer.json"
{
	"name": "joomla/install-from-web-server",
	"type": "project",
	"description": "Joomla! Install from Web Server",
	"keywords": ["joomla", "cms"],
	"homepage": "https://github.com/joomla-extensions/install-from-web-server",
	"license": "GPL-2.0-or-later",
	"require": {
		"php": "^7.2"
	},
	"require-dev": {
		"joomla/crowdin-sync": "dev-master"
	}
}
EOF

echo 'Installation successful!'
