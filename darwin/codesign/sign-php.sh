#!/bin/sh

if [ "$(uname -m)" = 'arm64' ]; then
	codesign --sign "Ange" --force --keychain ~/Library/Keychains/login.keychain-db /opt/homebrew/opt/php/lib/httpd/modules/libphp.so
else
	codesign --sign "Ange" --force --keychain ~/Library/Keychains/login.keychain-db /usr/local/opt/php/lib/httpd/modules/libphp.so
fi
