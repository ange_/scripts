Create a certificate authority for code signing
===============================================

# Creating a CA

1. Open Keychain access.app;
2. Keychain access -> Certificate Assistant -> Create a Certificate Authority;
3. In "Create Your Certificate Authority":
    a. Set the CA name;
    b. Identity Type: Self-Signed Root CA;
    c. User certificate: Code signing;
    d. Select "Let me override defaults";
    e. Select "Make this CA the default";
    f. Enter the email address for your CA;
    g. Click continue.
4. If a warning is encountered, ignore it;
5. Accept defaults for "Certificate Information" and click Continue (ignore any possible warning);
6. Enter the requested information and click Continue;
7. Accept defaults for "Key Pair Information For This CA" and click Continue;
8. Accept defaults for "Key Pair Information For Users of This CA" and click Continue;
9. Accept defaults for "Key Usage Extensions For This CA" and click Continue;
10. Accept defaults for "Key Usage Extensions For Users of This CA" and click Continue;
11. Click on "Include Extended Key Usage Extension";
12. Click to check the "Code Signing" checkbox;
13. Click Continue;
14. Accept defaults for "Extended Key Usage Extensions For Users of This CA" and click Continue;
15. Accept defaults for "Basic Constraints Extension For This CA" and click Continue;
16. Accept defaults for "Basic Constraints Extension For Users of This CA" and click Continue;
17. Accept defaults for "Subject Alternative Name For This CA" and click Continue;
18. Accept defaults for "Subject Alternative Name for Users of This CA" and click Continue;
19. Click Create to create the CA;
20. Close the "Certificate Assistant" window and open Keychain Access;
21. Double click on your newly created CA in login → My Certificates;
22. Click on the "Trust" tab;
23. Click on When using this certificate select list and select "Always trust". 

# Create a code signing certificate
1. Open Keychain access.app;
2. Keychain access -> Certificate Assistant -> Create a Certificate
3. In this mask set:
    a. The name of the certificate;
    b. Identity Type: Leaf;
    c. Certificate Type: Code Signing.
4. Select "CA or issuer" from the list and click "Create".
5. Click "Done".

# Signing the PHP module

`codesign --sign "Ange" --force --keychain ~/Library/Keychains/login.keychain-db /usr/local/opt/php/lib/httpd/modules/libphp.so`

# Credits
[Create a CA](https://www.simplified.guide/macos/keychain-ca-code-signing-create)
[Create a self-signed certificate](https://www.simplified.guide/macos/keychain-cert-code-signing-create)
