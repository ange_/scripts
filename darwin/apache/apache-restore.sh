#!/bin/sh

target_dir="/etc/apache2/"
config_file="${target_dir}httpd.conf"
userdir_file="${target_dir}extra/httpd-userdir.conf"

lines_to_uncomment=('LoadModule include_module libexec/apache2/mod_include.so' 'LoadModule vhost_alias_module libexec/apache2/mod_vhost_alias.so'
	'LoadModule userdir_module libexec/apache2/mod_userdir.so' 'LoadModule rewrite_module libexec/apache2/mod_rewrite.so'
	'LoadModule php7_module libexec/apache2/libphp7.so' 'Include /private/etc/apache2/extra/httpd-userdir.conf'
	'Include /private/etc/apache2/vhosts/*.conf'
)


# Usage: uncomment_line [LINE] [FILE(s)]
uncomment_line() {
	line=$1
	file=$2

	# -E enables extended regex. Here a group is defines with () and not \(\).
	#	Also, OR-operator is included.
	sed -i '.backup' -E "s/^#(${line})/\1/" "${file}"
}


# Uncommenting from /etc/apache2/httpd.conf
pattern=''
for line in "${lines_to_uncomment[@]}"; do
	escaped_line=$(echo "$line" | sed -E 's/(\/|\.)/\\\1/g')
	pattern+=${escaped_line}"|"
done

pattern=${pattern%?}
uncomment_line "${pattern}" "${config_file}"
grep "Include /private/etc/apache2/vhosts/\*.conf" "${config_file}" || printf "Include /private/etc/apache2/vhosts/*.conf\n" >> "${config_file}"


# Uncommenting from /etc/apache2/extra/httpd-userdir.conf
uncomment_line "Include \/private\/etc\/apache2\/users\/\*.conf" "${userdir_file}"