AppleScripts
============

This directory contains AppleScripts created (and/or collected) during my years of possessing a Mac.

Currently, it just contains: ```bulk-export-pdf-from-pages.applescript```.

In order to simplify this process, the files are provided as applescripts (thus decompiled): indeed, Script Editor will save them after a compilation step.

## Instructions
In order to compile an AppleScript file:
```
$ osacompile -o script_name.scpt script_name.applescript
```

It is still possible to decompile a scpt file:
```
$ osadecompile script_name.scpt
```

## bulk-export-pdf-from-pages
Given a directory of Pages' files, it will open them up, export them in PDF and close the file.
