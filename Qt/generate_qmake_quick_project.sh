#!/bin/sh


generate_qmake_quick_project() {
	project_name="$1"
	project_path="$PROJECTS_PATH/$project_name"
	pro_file="$project_path/$project_name.pro"
	qml_path="$project_path/qml"

	[ -d "$project_path" ] && echo "'$project_path' already exists." >&2 && exit 1

	# Project paths
	mkdir -p "$project_path"
	mkdir "$project_path/include"
	mkdir "$project_path/src"
	mkdir "$qml_path"

	# project.pro file
	cat <<- end-of-pro-file > "$pro_file"
	QT = core qml quick
	CONFIG += c++14

	HEADERS +=
	SOURCES += src/main.cc

	RESOURCES = qml/qml.qrc
	end-of-pro-file

	# qml.qrc
	cat << end-of-qrc > "$qml_path/qml.qrc"
<RCC>
	<qresource prefix="/">
		<file>main.qml</file>
	</qresource>
</RCC>
end-of-qrc

	# main.qml
	cat << EOF > "$qml_path/main.qml"
import QtQuick 2.0
import QtQuick.Controls 2.2

ApplicationWindow {
	width: 480
	height: width
	visible: true
}
EOF

	# main.cc
	cat << EOF > "$project_path/src/main.cc"
#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char **argv) {
	QGuiApplication app(argc, argv);
	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	return app.exec();
}
EOF
}


[ "$1" = '-h' ] && echo "usage: $(basename "$0") <project-name>" && exit 0
[ -z "$PROJECTS_PATH" ] && echo "'PROJECTS_PATH' is unset. Defaulting to '$(pwd)'" && PROJECTS_PATH=$(pwd) #PROJECTS_PATH="$HOME"
generate_qmake_quick_project "$@"
