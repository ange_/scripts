#!/bin/sh

generate_qmake_project() {
	project_name=$1
	PROJECTS_DIR=${PROJECTS_DIR:-$PWD}
	PROJPATH="$PROJECTS_DIR/$project_name"
	[ -d "$PROJPATH" ] && printf "Project already exists.\n" >&2 && exit 2
	mkdir -p "$PROJPATH"
	cat << EOF > "$PROJPATH/$project_name.pro"
QT -= gui

CONFIG += c++14 console
CONFIG -= app_bundle

SOURCES += \\
	main.cc
EOF

	# Gen main.cc
	printf "#include <QCoreApplication>\n\nint main(int argc, char **argv) {
	QCoreApplication a(argc, argv);

	return a.exec();\n}" > "$PROJPATH/main.cc"
	if [ -d ~/.vim/pack/lang-autocomplete/start/YouCompleteMe ]; then
		~/.vim/pack/lang-autocomplete/start/YCM-Generator/config_gen.py "$PROJPATH" --verbose
		sed -i'' "s/'-I.',/'-fPIC',\n    \0/" "$PROJPATH/.ycm_extra_conf.py"
	fi
}

generate_qmake_project $@
