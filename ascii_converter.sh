#!/bin/sh

usage="usage: $(basename "$0") <option>

Options:
	-i int		converts the integer into a character.
	-c character 	converts the character into its integer representation."

int_to_char() {
	int="$1"
	printf "\x$(printf '%x' "$int")\n"
}

char_to_int() {
	char="$1"
	printf '%d\n' "'${char}"
}

[ "$#" -eq 0 ] && echo "$usage" >&2 && exit 1
while getopts 'hc:i:' opt; do
	case "$opt" in
		c)
			char_to_int "$OPTARG"
			;;
		i)
			int_to_char "$OPTARG"
			;;
		*)
			echo "$usage"
			;;
	esac
done
