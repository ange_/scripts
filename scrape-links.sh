#!/bin/bash

if [ $# -ne 1 ]; then
	echo "Incorrect Input." >&2
	echo "usage: $(basename "$0") <url>"
	exit 1
fi

which lynx >/dev/null 2>&1 || (echo "This script requires lynx." >&2 && exit 2)

linksFile="$HOME/Desktop/links.txt"
tmpFile="$HOME/Desktop/tmp.txt"

echo > "$linksFile"
rm -f "$tmpFile"

lynx -dump -image_links -listonly "$1" | awk '{ print $2 }' > "$tmpFile"

lines=$(wc -l "$tmpFile" | awk '{ print $1 }')

for i in $(seq 1 "$lines"); do
	newLine=$(sed -n "${i}p" "$tmpFile")
	if [[ $newLine == http://www.exashare.com/* || $newLine == https://oload.co/* || $newLine == http://speedvideo.net/* ]]; then
		echo "$newLine" >> "$linksFile"
	fi
done

echo "Found: $(wc -l "$linksFile" | awk '{ print $1 }') Episodes."
rm -f "$tmpFile"
exit 0
