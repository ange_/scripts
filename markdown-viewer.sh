#!/bin/sh

for dependency in pandoc lynx; do
	if ! command -v "$dependency" >/dev/null 2>&1; then
		echo "Error: missing dependency '$dependency'." >&2
		exit 1
	fi
done

mdview() {
	file="$1"
	pandoc "$file" | lynx -stdin
}

mdview "$@"
