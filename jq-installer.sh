#!/bin/sh

get_filename() {
	arch="$(uname -m)"
	os="$(uname | tr '[:upper:]' '[:lower:]')"

	[ "$arch" = 'x86_64' ] && arch='amd64'

	if [ "$os" = 'darwin' ]; then
		os='macos'
	fi

	echo "jq-${os}-${arch}"
}

remote="$(curl -L https://api.github.com/repos/jqlang/jq/releases/latest | grep "$(get_filename)")"
url="$(echo "$remote" | awk -F'"' '/http/ { print $(NF - 1); }')"

cd ~/bin || exit 1
curl -LJo jq "$url"
chmod u+x jq
