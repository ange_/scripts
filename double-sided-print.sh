#!/bin/sh

while getopts "hb:e:n:o" opt; do
	case "$opt" in
		"b")
			begin="$OPTARG"
			;;
		"e")
			end="$OPTARG"
			;;
		"h")
			printf "Usage: %s [-b begin_page] [-e end_page] [-h]\n" "$(basename "$0")"
			exit 0
			;;
		"n")
			copies="$OPTARG"
			;;
		*)
			;;
	esac
done

shift "$((OPTIND - 1))"

PDF_NAME="$1"
[ -z "$PDF_NAME" ] && echo "Missing PDF file name" >&2 && exit 1
[ -z "$begin" ] && begin=1
if [ -z "$end" ]; then
	[ "$(uname)" = "Darwin" ] && end="$(mdls -name kMDItemFSName -name kMDItemNumberOfPages "$PDF_NAME" | awk 'END{print $NF}')" || end=1

fi

[ -z "$copies" ] && copies=1

pages_to_print="$(seq -s ',' -t $'\b' "$begin" 2 "$end")"
printf "The following %s pages are going to be printed:\n%s.\nDo you want to proceed? [Y/n] " "$(let "$begin%2 == 0" && echo "EVEN" || echo "ODD")"  "$pages_to_print"
read -r confirm

[ "$confirm" = 'n' ] || [ "$confirm" = 'N' ] || [ "$(echo "$confirm" | awk '{print length}')" -ne 1 ] && exit 0

lp "$PDF_NAME" -P "$pages_to_print" -n "$copies"
