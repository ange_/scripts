#!/usr/bin/env python

from argparse import ArgumentParser
from sys import stderr, argv

parser = ArgumentParser()

parser.add_argument("data", type=str, nargs="*")
parser.add_argument("-c", "--cast_char", help="Cast a char to an integer", action="store_true")
parser.add_argument("-i", "--cast_integer", help="Cast an integer to a char", action="store_true")

args = parser.parse_args()

argc = len(argv) - 2

# argc has value "-1" when no args are given
if argc == -1 or not args.data:
	parser.print_usage()
	exit(0)

if args.cast_char:
	#print "I will cast a given char to an Integer"
	try:
		for i in range(0,argc):
			print ord(args.data[i])
	except TypeError:
		print >>stderr, "cast_char needs a char. Did you mean to use -i?"
		exit(1)

elif args.cast_integer:
	#print "I will cast a given Integer to a Char"
	try:
		for i in range(0, argc):
			print chr(int(args.data[i]))
	except ValueError:
		print >>stderr, "cast_integer needs an integer. Did you mean to use -c?"
		exit(1)
else:
	print >>stderr, "Input Error. No cast selected."
	exit(1)
