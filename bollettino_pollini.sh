#!/bin/sh

for dependency in curl xmllint tr sed; do
	if ! command -v "$dependency" >/dev/null 2>&1; then
		echo "Missing: curl" >&2
		exit 1
	fi
done

printf "Polline\t\tLun\tMar\tMer\tGio\tVen\tSab\tDom\tCarica\n"
curl -s https://ambiente.provincia.bz.it/aria/bollettino-pollini-bolzano.asp 2>/dev/null | xmllint --html --xpath "//div[@id='main_container']/div[@id='content']/table[contains(@class, 'valori')]/tbody/tr/td[@class='famiglia' and (text() = 'Graminacee' or text() = 'Urticacee')]/ancestor::tr" - 2>/dev/null | sed -e 's/\(>\)\(<\/td>\)/\1NON DISPONIBILE\2/g' -e 's/\(<\/td>\)/\\n\1/g' | xmllint --html --xpath '//tr/td/text() | //tr/td/img/@alt' - | tr -d '\r' | tr -d '\0' | while read -r line; do
	if echo "$line" | grep -qE '\n?(Urticacee|Graminacee)'; then
		begin=$(printf "%.2s" "$line")
		[ "$begin" = '\n' ] && line=${line#??}
		echo "$line" | tr '\n' '\t'
		echo
	fi
done
