#!/bin/sh 

[ "$(id -u)" -ne 0 ] && echo 'This script must be run as root.' >&2 && exit 1
usage() {
	cat << EOF
usage: $(basename "$0") OPTIONS none|kde|xfce|openbox|raspberry|raspberrygui
Options
	-b	install bluetooth support
	-h	shows this help message
	-v	use neovim
EOF
}

script_path="$(dirname "$0")" 
while getopts "hvbd" opt; do
	case "$opt" in
		'h')
			usage
			exit 0
			;;
		'b')
			HAS_BLUETOOTH=1
			;;
		'd')
			DEV_TOOLS=1
			;;
		'v')
			USE_NVIM=1
			;;
		*)
			usage >&2
			exit 1
			;;
	esac
done

shift "$((OPTIND - 1))"

# Debian DEs
install_kde() {
	apt-get install -y kde-plasma-desktop gwenview kde-config-gtk-style kde-config-sddm sddm kscreen kwin-x11 kwallet-pam okular partitionmanager plasma-nm plasma-pa qml-module-org-kde-newstuff yakuake ksysguard
	[ -n "$HAS_BLUETOOTH" ] && apt-get install -y bluedevil
}

install_xfce() {
	apt-get install -y xfce4 xfce4-goodies greybird-gtk-theme	# TODO: Remove Xfburn if CD-drive is not detected
	xfconf-query -c xfce4-keyboard-shortcuts -p /commands/custom/F1 -t "string" -s "xfce4-terminal --drop-down" -n
}

install_openbox() {
	apt-get install -y openbox obconf tint2 nm-applet thunar rxvt-unicode ristretto
}

install_kodi() {
	apt-get install -y kodi libwidevinecdm0 libgl1-mesa-dri
	useradd -b /var/lib -c 'Kodi restricted account.' -G audio,input,video,disk,tty,plugdev -mU kodi
	passwd -d kodi
	cat <<- EOF | sudo -u kodi tee /var/lib/kodi/kodi.sh
		#!/bin/sh
		/usr/bin/openvt -sw -- /usr/lib/arm-linux-gnueabihf/kodi/kodi.bin --standalone kodi
		EOF
	su kodi -c 'chmod 500 ~/kodi.sh'
	cat <<- EOF | sudo -u kodi tee /var/lib/kodi/kodi.service
		[Unit]
		Description=Kodi Media Center (standalone user)
		After=network-online.target
		Wants=network-online.target

		[Service]
		User=kodi
		Group=kodi
		Type=simple
		ExecStart=/var/lib/kodi/kodi.sh
		ExecStop=pkill -9 kodi.bin
		Restart=always

		[Install]
		WantedBy=graphical.target
		EOF
	systemctl enable /var/lib/kodi/kodi.service
	systemctl set-default graphical.target
}

# Parsing args
TARGET='desktop'
case "$1" in
	'none')
		install_desktop=
		;;
	'kde')
		install_desktop=install_kde
		;;
	'xfce')
		install_desktop=install_xfce
		;;
	'openbox')
		install_desktop=install_openbox
		;;
	'raspberry')
		install_desktop=
		TARGET='raspberry'
		;;
	'raspberrygui')
		install_desktop=install_kodi
		TARGET='raspberry'
		;;
	*)
		if [ "$#" -le 1 ]; then
			printf 'Invalid option. See %s -h\n' "$(basename "$0")" >&2
		else
			printf "%s is not supported." "$1" >&2
		fi
		exit 2
esac

# Configuring APT: stable + pinning to Sid
echo 'APT::Install-Recommends "0";' > /etc/apt/apt.conf.d/02no-recommends
if [ "$TARGET" = 'desktop' ]; then
	echo 'deb http://ftp.it.debian.org/debian/ sid main' | tee /etc/apt/sources.list.d/sid.list
	cat <<- EOF > /etc/apt/preferences.d/90pinning
	Package: *
	Pin: release n=sid
	Pin-Priority: -1

	Package: firefox
	Pin: release n=sid
	Pin-Priority: 501
	EOF
	apt-get update
fi

# Installing base programs
apt-get install -y $(sed '1q;d' "${script_path}/debian-programs.txt")
[ -n "$USE_NVIM" ] && [ "$USE_NVIM" -eq 1 ] && apt-get install -y neovim

if command -v apt-file >/dev/null 2>&1; then
	apt-file update
fi

# Installing the video graphics drivers
if [ "$TARGET" = 'desktop' ]; then
	_out="$(lspci -v | grep VGA)"
	if echo "$_out" | grep -o "Intel" >/dev/null 2>&1; then
		apt-get install -y xserver-xorg-video-intel
	elif echo "$_out" | grep -o "ATI" >/dev/null 2>&1; then
		apt-get install -y xserver-xorg-video-amdgpu
	fi

	# If is a laptop, install the touchpad's driver
	laptop-detect && apt-get install -y xserver-xorg-input-libinput
fi

# Check if is bluetooth-capable
[ -z "$HAS_BLUETOOTH" ] && if dmesg | grep -i bluetooth | grep "Product: "; then
	HAS_BLUETOOTH=1
fi

# Installing desktop
$install_desktop

# Installing dev tools
[ -n "$DEV_TOOLS" ] && apt-get install -y "$(sed '2q;d' "${script_path}/debian-programs.txt")"

# Installing browser and base programs
if [ "$TARGET" = 'desktop' ]; then
	apt-get install -y "$(sed '3q;d' "${script_path}/debian-programs.txt")"
	apt-get install -y "$(sed '4q;d' "${script_path}/debian-programs.txt")"
fi

echo 'Add your user to "netdev" to operate with networking.'
cat << EOF | tee /etc/polkit-1/rules.d/50-org.freedesktop.NetworkManager.rules
polkit.addRule(function(action, subject) {
	if (action.id.indexOf("org.freedesktop.NetworkManager.") == 0 && subject.isInGroup("netdev")) {
		return polkit.Result.YES;
	}
});
EOF
