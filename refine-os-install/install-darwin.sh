#!/bin/sh

[ "$(id -u)" -eq 0 ] && echo 'This script must be run as non-root user' >&2 && exit 1

log() {
	status=$1
	shift
	text=$*
	[ "$status" -ne 0 ] && status="Failed" || status="OK"
	printf "[ %s ] %s\n" "$status" "$text"
}

err() {
	status="$1"
	shift
	log "$status" "$*" >&2
	exit "$status"
}

get_content_length() {
	url="$*"
	curl -LI "$url" | awk -F: '/content-length/ { gsub(/ |\r/, "", $2); print $2; }'
}

check_size() {
	file="$1"
	remote="$2"
	application_name="${file%.*}"
	[ "$(wc -c "$file" | cut -d' ' -f2)" -ne "$(get_content_length $remote)" ] && err 10 "File size for application \"$application_name\" does not match with remote"
	return 0
}

alias install_zip="unzip -d \"$APPLICATIONS_INSTALL_PATH\""

usage() {
	cat << EOU
usage: $(basename "$0") OPTIONS

Options:
	-b	install Homebrew.
	-d	installs developer tools.
	-h	shows this message.
	-m	install MacPorts.
	-w	install webdev tools (requires either Homebrew or MacPorts).
EOU
}


while getopts 'hbmdw' opt; do
	case "$opt" in
		'h')
			usage && exit 0
			;;
		'b')
			[ -n "$USE_MACPORTS" ] && echo 'Overriding "USE_MACPORTS" environment variabile: -b installs Homebrew.' && unset USE_MACPORTS
			USE_HOMEBREW=1
			pkg_installer_cmd='brew'
			;;
		'm')
			[ -n "$USE_HOMEBREW" ] && echo 'Overriding "USE_HOMEBREW" environment variabile: -b installs MacPorts.' && unset USE_HOMEBREW
			USE_MACPORTS=1
			pkg_installer_cmd='sudo port'
			;;
		'd')
			xcode-select --install
			;;
		'w')
			WEBDEV_ENV=1
			;;
	esac
done

[ -n "$WEBDEV_ENV" ] && [ -z "$USE_HOMEBREW" ] && [ -z "$USE_MACPORTS" ] && \
	echo 'In order to install the web development environment, MacPorts or Homebrew must be selected.' >&2 \
	&& exit 10


INSTALLERS_PATH="${INSTALLERS_PATH:-$HOME/Downloads}"
INSTALL_PATH="${INSTALL_PATH:-$HOME/bin}"
APPLICATIONS_INSTALL_PATH="${APPLICATIONS_INSTALL_PATH:-$HOME/Applications}"
[ ! -d "$INSTALL_PATH" ] && mkdir -p "$INSTALL_PATH" && echo "'$INSTALL_PATH' created."

## Installing applications
cd "$INSTALLERS_PATH" || exit 20

# Installing Firefox
printf 'Installing Firefox\n\n'
curl -LJo Firefox.dmg "https://download.mozilla.org/?product=firefox-latest-ssl&os=osx&lang=it"
check_size 'Firefox.dmg' "https://download.mozilla.org/?product=firefox-latest-ssl&os=osx&lang=it"
hdiutil attach Firefox.dmg || err 11 'Unable to mount "Firefox.dmg".'
cp -Rav /Volumes/Firefox/Firefox.app "$APPLICATIONS_INSTALL_PATH"
hdiutil detach /Volumes/Firefox

# Installing BetterZip
printf '\nInstalling BetterZip\n\n'
curl -LJO https://macitbetter.com/BetterZip.zip
check_size 'BetterZip.zip' 'https://macitbetter.com/BetterZip.zip'
unzip -d "$APPLICATIONS_INSTALL_PATH" BetterZip.zip

# Installing AppCleaner
latest_link="$(curl https://freemacsoft.net/appcleaner/ | xmllint --html --xpath 'string(//div[@class="downloads"]/ul/li[1]/a/@href)' - 2>/dev/null)"
zip_name="$(basename "$latest_link")"
curl -LJO "$latest_link"
check_size "$latest_link"
install_zip "$zip_name"


# Install external package manager
if [ -n "$USE_HOMEBREW" ]; then
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
elif [ -n "$USE_MACPORTS" ]; then
	macos_version="$(awk '/SOFTWARE LICENSE AGREEMENT FOR macOS/' '/System/Library/CoreServices/Setup Assistant.app/Contents/Resources/en.lproj/OSXSoftwareLicense.rtf' | awk -F 'macOS ' '{print $NF}' | awk '{print substr($0, 0, length($0)-1)}')"
	remote=$(curl -L https://api.github.com/repos/macports/macports-base/releases/latest | jq --arg version "-${macos_version}" '.assets[] | select(.name | contains($version) and (endswith(".asc") | not)) | {name,browser_download_url}')
	installer_name="$(echo "$remote" | jq .name)"
	echo "$remote" | jq .browser_download_url | xargs curl -LJO -
	installer -pkg "${installer_name}" -target /
fi
