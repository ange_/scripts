#!/bin/sh

[ "$(id -u)" -ne 0 ] && echo "This script must be run as root." >&2 && exit 1

log() {
	status=$1
	shift
	text=$*
	[ "$status" -ne 0 ] && status="Failed" || status="OK"
	printf "[ %s ] %s\n" "$status" "$text"
}

usage() {
	cat << EOU
usage: $(basename "$0") OPTIONS
Options:
	-d		install developer tools
	-h		shows this help message
	-p		delete user password
	-u USER 	specify end user
	-v		VM environment (so deletes useless packages)
EOU
}

CALLER="$SUDO_USER"
script_path="$(dirname "$0")" 
while getopts 'hdpvwu:' opt; do
	case "$opt" in
		'h')
			usage
			exit 0
			;;
		'd')
			DEV=1
			;;
		'p')
			[ -z "$CALLER" ] && echo 'To delete the user password, you must specify -u USER first' >&2 && exit 2
			passwd -d "$CALLER"
			;;
		'v')
			VM_ENV=1
			;;
		'u')
			CALLER="$OPTARG"
			;;
		*)
			usage >&2
			exit 1
			;;
	esac
done
shift "$((OPTIND - 1))"

# Remove snap
if [ -z "$KEEP_SNAP" ]; then
	while [ "$(snap list | wc -l)" -gt 2 ]; do
		for pkg in $(snap list | awk '{ print $1; }'); do
			[ "$pkg" != 'snapd' ] && snap remove --purge "$pkg"
		done
	done

	snap remove --purge snapd
	systemctl mask snapd
	apt-mark hold snapd
	cat <<- EOF >/etc/apt/preferences.d/nosnap.pref
	Package: snapd
	Pin: release a=*
	Pin-Priority: -10
	EOF
	apt-get autoremove --purge snapd
	rm -rf "$HOME/snap"
	rm -rf /snap /var/snap /var/lib/snapd
	log 0 "Snap removed"
fi

# Remove packages
[ -n "$VM_ENV" ] && (apt-get autoremove --purge thunderbird libreoffice*; log "$?" "Removing useless VM packages")

# Update the system
apt-get update
apt-get upgrade
log "$?" "System updated"

# Install firefox PPA if we are not keeping snap
if [ -z "$KEEP_SNAP" ]; then
	add-apt-repository ppa:mozillateam/ppa
	cat <<- EOF > /etc/apt/preferences.d/firefox.pref
	Package: firefox
	Pin: release o=LP-PPA-mozillateam
	Pin-Priority: 1001
	EOF
	apt-get update
	apt-get install -y firefox
	log "$?" "Install Firefox"
fi

# Install base programs
apt-get install -y $(sed '1q;d' "${script_path}/debian-programs.txt")
log "$?" "Install base system"
if [ -n "$DEV" ]; then
	apt-get install -y $(sed '2q;d' "${script_path}/debian-programs.txt")
	log "$?" "Install dev system"
fi

if [ -z "$VM_ENV" ]; then
	apt-get install -y $(sed '3q;d' "${script_path}/debian-programs.txt")
	apt-get install -y $(sed '4q;d' "${script_path}/debian-programs.txt")
fi

# Install essential elements
if dpkg-query -s xubuntu-core >/dev/null 2>&1; then
	apt-get install -y xfce4-eyes-plugin xfce4-clipman-plugin xarchiver	# Maybe engrampa?
	log "$?" "Installed additonal Xubuntu packages"
	sudo -u "$CALLER" xfconf-query -c xfce4-keyboard-shortcuts -p /commands/custom/F3 -t "string" -s "xfce4-terminal --drop-down --hide-menubar" -n
	log "$?" "Drop-down terminal configured"
elif dpkg-query -s kubuntu-desktop >/dev/null 2>&1; then
	apt-get install -y yakuake ksysguard
	log "$?" "Installed additional Kubuntu packages"
fi

command -v apt-file >/dev/null 2>&1 && apt-file update
