#!/bin/bash
#I'll use some bash-isms, so don't check for POSIX compatibility 

PROJECTSPATH=${PROJECTSPATH:-$HOME/Scrivania}
FD=9	# 9 is probably unused

exec 9>&1
exec 1> >(paste /dev/null -)

for proj in "$PROJECTSPATH"/*/; do
	[ -d "$proj/.git/" ] && printf "Updating %s\n" "$(basename $proj)" >&9 && cd "$proj" && git pull && cd ..
done

exec 1>&9 9>&-
