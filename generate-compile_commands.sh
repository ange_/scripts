#!/bin/sh

CC=${CC:-c++}
CC="$(command -v "$CC")"
STD=${STD:-c++14}
[ -z "$QT_HOME" ] && echo '$QT_HOME unset!' >&2 && exit 1
qt="$(find "$QT_HOME" -name clang_64 -maxdepth 2 2>/dev/null)"
[ -z "$qt" ] && echo 'No Qt installation found: aborting.' >&2 && exit 2

if [ "$1" = '-h' ]; then
	cat << EOF
usage: $(basename "$0") <project-dir> [main.cc]
This script generates an essential compile_commands.json given a project path.

EOF
fi

project_path="$1"

if [ ! -d "$project_path" ]; then
	echo "\"$project_path\" is not a directory." >&2
	exit 20
fi

if [ "$(uname)" = 'Linux' ]; then
	cat << EOF
	[{
		"arguments": [
			"$CC",
			"-std=$STD",
			"-I.",
			"-I$qt/include",
			"-I$qt/include/QtCore",
			"-I$qt/include/QtGui",
			"-I$qt/include/QtQml",
			"-I$qt/include/QtNetwork",
			"-I$qt/mkspecs/linux-g++"
		],
		"directory": "$(readlink -f "$project_path")",
		"file": ""
	}]
EOF
elif [ "$(uname)" = 'Darwin' ]; then
	cat << EOF | tee compile_commands.json
	[{
		"arguments": [
			"$CC",
			"-std=$STD",
			"-I.",
			"-F$qt/lib",
			"-I$qt/lib/QtCore.framework/Headers/",
			"-I$qt/lib/QtGui.framework/Headers/",
			"-I$qt/lib/QtQml.framework/Headers/",
			"-I$qt/lib/QtWidgets.framework/Headers/",
			"-I$qt/lib/QtNetwork.framework/Headers/"
		],
		"directory": "$(readlink -f "$project_path")",
		"file": ""
	}]
EOF
fi
